#!/usr/bin/env bash
# License: GPLv3
set -euo pipefail
IFS="$(printf '\n\t')"

readonly version="1.0.0"
readonly scriptname="${0##*/}"
function printUsage()
{
    echo "${scriptname} ${version}"
    echo "Usage: ${scriptname} [OPTION]... [FLAG]..."
    echo "Warframe alerts notifications for desktops"
    echo "Example: ${scriptname} -p ps4 &"
    echo
    echo "Options:"
    echo "  -p, --platform              Target platform, valid values: ps4 xb1 pc"
    echo
    echo "Flags:"
    echo "  -1  --onetime               Run script without polling, notifies for the latest alert"
    echo "  -h, --help                  Show this help"
    exit 0
}

readonly urlWorldState="https://ws.warframestat.us/"

# set default values
platform="ps4"
help=false
onetime=false

declare -a positional=()

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -p|--platform)
    platform="$2"
    shift # past argument
    shift # past value
    ;;
    -h|--help)
    help=true
    shift # past argument
    ;;
    -1|--onetime)
    onetime=true
    shift # past argument
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if [[ "$help" == true ]]; then
    printUsage
    exit
fi;

function notify()
{
    if [[ -t 1 ]]; then echo "#### $(basename -- "$0") notification #### -> $1"
    else # in x, notifications
        if   hash notify-send 2>/dev/null; then notify-send "$1"
        elif hash zenity 2>/dev/null; then { echo "message:$1" | zenity --notification --listen & }
        elif hash kdialog 2>/dev/null; then { kdialog --title "$1" --passivepopup "This popup will disappear in 5 seconds" 5 & }
        elif hash xmessage 2>/dev/null; then xmessage "$1" -timeout 5
        else echo "[$(date -u +"%FT%TZ")] $1" > "${HOME}/$(basename -- "$0")_notify.txt"
        fi
    fi
}

worldstate=$(mktemp)

function downloadWorldState()
{
    wget -nv -O - "${urlWorldState}${1}" > "$worldstate"
}

function cleanup() {
    echo "Cleaning stuff up..."
    rm -fv "$worldstate"
    exit
}

trap cleanup INT TERM ERR

lastId=""



while true
do
    downloadWorldState "$platform"

    newest=$(
    jq -S "[.alerts[] | {
        id: .id,
        activation: .activation,
        reward: .mission.reward.asString,
        where: .mission.node,
        type: .mission.type} ]
        |
        sort_by(.activation)
        |
        .[-1]
    " < "$worldstate"
    )

    newestId=$(echo "$newest" | jq -r ".id")
    where=$(echo "$newest" | jq -r ".where")
    reward=$(echo "$newest" | jq -r ".reward")
    gamemode=$(echo "$newest" | jq -r ".type")


    if [[ "$newestId" != "$lastId" ]];
    then
        notify "Warframe Alert: ${reward} @ ${where} (${gamemode})"
    fi

    lastId=$newestId

    if [[ "$onetime" == true ]];
    then
        break
    fi

    sleep 30s
done

cleanup