#!/usr/bin/env bash
#
# Set up anacron for the current user.
# Based on: https://grinux.wordpress.com/2012/04/18/run-anacron-as-user/
#
set -euo pipefail
IFS="$(printf '\n\t')"

function checkTools()
{
    local -i missing=0
    for cmd; do
        if ! hash "$1" >/dev/null 2>&1 "$cmd" ; then
            error "$cmd is missing"
            missing=1
        fi
    done

    if  [[ $missing -ne 0 ]]; then
        error "Install missing dependencies listed above."
        exit 103
    fi
}

checkTools anacron

function error()
{
  echo "[Error]: $*" >&2
}

anacrondir="$HOME/.anacron"

if [[ -d "$anacrondir" ]]; then
    error "anacron directory $anacrondir already exists. Abort."
    exit 101
fi

cd "$HOME"
mkdir -p "$anacrondir"
cd "$anacrondir"
mkdir cron.daily cron.weekly cron.monthly spool etc

anacrontab="$anacrondir/etc/anacrontab"
anacronspool="$anacrondir/spool"
touch "$anacrontab"

{
echo "# configuration file for anacron"
echo "# See anacron(8) and anacrontab(5) for details."
echo "SHELL=/bin/sh"
echo "PATH=$HOME/.local/bin:$HOME/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin"
echo "LOGNAME=$USER"
echo "#MAILTO=<email>"
echo "# These replace cron's entries"
echo "1       3       cron.daily      run-parts --report $anacrondir/cron.daily"
echo "7       7       cron.weekly     run-parts --report $anacrondir/cron.weekly"
echo "@monthly        11      cron.monthly    run-parts --report $anacrondir/cron.monthly"
} >> "$anacrontab"

anacron -T -t "$anacrontab"

echo "anacron was setup in $anacrondir"
echo "Add the following line in your .profile or autostart:"
echo "  anacron -t $anacrontab -S $anacronspool "
echo ""
echo "Copy your scripts or link them to the cron.* directories."
echo "Script files must have 755/764 permissions."
echo "Done."
