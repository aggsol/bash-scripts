# Backup

Hard code the settings in the backup scripts and use a copy on the medium itself.

## Backup Home Folder

1. Copy `backup-home.sh` and `ignorelist` to USB stick
2. Edit files on the USB stick
    - Set `$backupdir` to a folder on the USB stick
    - Set `$ignorelist` to the path of the ignore list file
3. Execute `backup-home.sh` for a dry run
4. Execute backup with option `--production`


## Duplicity Remote Backup

1. Copy `duplcity-helper.sh` and `ignorelist-helper` to your `$HOME`
2.

## Tips

* Use `anacron` to schedule backups