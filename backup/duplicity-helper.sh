#!/usr/bin/env bash
set -u
IFS="$(printf '\n\t')"

readonly version="v1.0.0"
readonly scriptname="${0##*/}"

### BEGIN USER CONFIG
# Hard code your backup process here once.
#
# This file contains the passphrase for the backup:
#  PASSPHRASE="<verylongandrandom>
readonly passfile="$HOME/.backup-passphrase"

# Folder to be backed up
readonly dataPath="/home/ubuntu/tmp/";

# Backup location as URL
readonly backupUrl="file:///tmp/backup/";

# Logfile created by duplcity
readonly logfile="$PWD/${scriptname}.log"

# File with folders to ignore
ignorelist="$PWD/ignorelist-duplicity"

### END USER CONFIG

verbose=false
readonly show_debug=true
function debug() {
    if $show_debug; then
        echo -e "DEBUG: $*" | tr -s ' ' | fmt -100
    fi
}

function info() {
    if $verbose; then
        echo -e "$@" | tr -s ' ' | fmt -100
    fi
}

function error() {
    echo -e "Error: $*" | tr -s ' ' | fmt -100 1>&2
}

function usage()
{
cat << EOF
$scriptname $version
Helper for easy backups with duplicity. Edit $scriptname before first usage!
Usage:
    $scriptname [MODE] [OPTIONS] [--production]

Modes:
    backup                              Backup \$dataPath to \$backupUrl
    verify                              Verify backup
    restore <file-to-restore> <dest>    Restore file to the given path
    full-restore <dest>                 Complete restore from backup to destination

Options:
        --production            Execute process, default is a dry run.
    -i, --ignorelist <file>     Ignore list file
    -h, --help                  Display this help message
        --version               Display version
    -v, --verbose               Additional output

Default settings can be set at the top of the script in the 'USER CONFIG' section.
The 'USER CONFIG' section has additional settings that have no command line flags.

Reading $scriptname is recommended!

EOF
}

# Read passphrase from file and set an env var
function readPassphrase()
{
    if [[ ! -f "$passfile" ]]; then
      error "Passphrase file is not a valid file. Abort."
      exit 101
    fi

    if [[ $(stat -c %a "$passfile") != 600 ]]; then
      error "Passphrase file has wrong permissions. Expected 600. Abort."
      exit 102
    fi

    source "$passfile" &> /dev/null
    export PASSPHRASE
}

# Cleanup in every case
function cleanup() {
    unset PASSPHRASE
}

trap cleanup INT TERM ERR

# set default values
dryRun="--dry-run"

declare -a positional=()

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --production)
    readonly dryRun="--progress"
    shift
    ;;

    -v|--verbose)
    readonly verbose=true
    shift
    ;;


    -h|--help)
    usage
    exit 0
    shift # past argument
    ;;

    -i|--ignorelist)
    readonly ignorelist="$2"
    if [[ ! -f "$ignorelist" ]]; then
        echo "Invalid ignore list: $ignorelist"
        exit 102
    fi;
    shift # past argument
    shift # past value
    ;;

    --version)
    echo "$version"
    exit 0
    shift # past argument
    ;;

    *)    # unknown options
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

readPassphrase

options="$dryRun --log-file $logfile"

readonly mode=${positional[0]:-}
debug "mode: $mode"
debug "options: ${options}"
debug "dataPath :${dataPath}"
debug "backupUrl :${backupUrl}"

readonly second=${positional[1]:-}
readonly third=${positional[2]:-}
cmd=""
case $mode in
    backup)

    if [[ -f "$ignorelist" ]]; then
        info "Using ignore list: $ignorelist"
        options="$options --exclude-filelist $ignorelist"
    fi
    cmd="duplicity --full-if-older-than 6M ${options} ${dataPath} ${backupUrl}"
    ;;

    list-files|ls)
    dryRun=""
    cmd="duplicity list-current-files $backupUrl"
    ;;

    verify)
    dryRun=""
    cmd="duplicity -v4 verify $backupUrl $dataPath"
    ;;

    restore)
    if [[ -z "$second" || -z "$third" ]]; then
        echo "Missing parameters"
        exit 103
    fi
    cmd="duplicity --file-to-restore ${second} ${backupUrl} ${third}"
    ;;

    full-restore)
    cmd="duplicity restore --progress ${backupUrl} ${second}"
    ;;

    *)
    echo "Invalid mode."
    echo "Try -h/--help"
    echo "READ $scriptname BEFORE FIRST USAGE!"
    exit 104
    ;;
esac

if [[ "$dryRun" == "--dry-run" ]]; then
    echo "*** DRY RUN ***"
elif [[ "$dryRun" == "--progress" ]]; then
    echo "*** PRODUCTION BACKUP ***"

fi

info "Command: ${cmd}"
eval "$cmd"
