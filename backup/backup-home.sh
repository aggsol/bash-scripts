#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

readonly version="1.0.0"
readonly scriptname="${0##*/}"
function printUsage()
{
    echo "${scriptname} ${version}"
    echo "Usage: ${scriptname} [OPTION]... [FLAG]..."
    echo "Backup your home directory with rsync using an ignorelist"
    echo "Example: ${scriptname} -b /media/ -i ./ignorelist "
    echo
    echo "Options:"
    echo "  -i, --ignorelist <path>     Path to the ignore list file"
    echo "  -b, --backupdir <path>      Destination for the backup"
    echo
    echo "Flags:"
    echo "      --production            Execute the actual backup"
    echo "  -h, --help                  Show this help"
    exit 0
}

# set default values
production=false
help=false
ignorelist="/media/$USER/XMG_BACKUP/ignorelist"
backupdir="/media/$USER/XMG_BACKUP/Backup"

# input par
declare -a positional=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--ignorelist)
    readonly ignorelist="$2"
    shift # past argument
    shift # past value
    ;;
    -b|--backupdir)
    readonly backupdir="$2"
    shift # past argument
    shift # past value
    ;;
    --production)
    readonly production=true
    shift # past argument
    ;;
    -h|--help)
    readonly help=true
    shift # past argument
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if [[ "$help" == true ]]; then
    printUsage
fi;

function error()
{
  echo "[Error]: $*" >&2
}

if [[ ! -d "$backupdir" ]]; then
    error "Invalid backup destination: $backupdir"
    exit 101
fi

if [[ ! -f "$ignorelist" ]]; then
    error "Invalid ignore list: $ignorelist"
    exit 102
fi;

function notify()
{
    if [[ -t 1 ]]; then echo "#### $(basename -- "$0") notification #### -> $1"
    else # in x, notifications
        if   hash notify-send 2>/dev/null; then notify-send "$1"
        elif hash zenity 2>/dev/null; then { echo "message:$1" | zenity --notification --listen & }
        elif hash kdialog 2>/dev/null; then { kdialog --title "$1" --passivepopup "This popup will disappear in 5 seconds" 5 & }
        elif hash xmessage 2>/dev/null; then xmessage "$1" -timeout 5
        else echo "[$(date -u +"%FT%TZ")] $1" > "${HOME}/$(basename -- "$0")_notify.txt"
        fi
    fi
}

if [[ "$production" == true ]]; then
    logfile="${backupdir}/${scriptname}.log"
    touch "$logfile"
    timestamp=$(date -u +"%FT%TZ")
    echo "${timestamp} Start backup to ${backupdir}" >> "$logfile"

    rsync -aP --exclude-from="$ignorelist" "/home/$USER/" "$backupdir"

    timestamp=$(date -u +"%FT%TZ")
    echo "${timestamp} Backup successful!" >> "$logfile"

    notify "Backup successful."
    echo "Backup done."
else
    rsync -vnaP --exclude-from="$ignorelist" "/home/$USER/" "$backupdir"
    echo "Test run done. Use flag --production to execute backup."
fi

