# Yet Another Bash Script Collection

My bash scripts derived from all over the place.

## Content

| Name            | Description                                |
| ---             | ---                                        |
| anacron-user    | Setup anacron for a local user             |
| audio-play      | Download audio plays                       |
| authenticator   | User andOTP JSON file for 2FA              |
| backup          | Back up home folder                        |
| bingwallpaper   | Load the current Bing wallpaper            |
| cubus-backup    | Easy wrapper for borg backups              |
| onedrive-backup | Backups to OneDrive with Rclone and Restic |
| git-crypto      | Encrypt a git repository                   |
| khadd           | M! Forum Skripte                           |
| laptop          | Laptop related scripts                     |
| password        | Generate German passwords                  |
| templates       | Parameters, notifications                  |
| warframe        | Notifications for Warframe alerts          |

## Bookmarks

### Other Scripts

* [gitwatch](https://github.com/gitwatch/gitwatch)
* [bitpocket](https://github.com/sickill/bitpocket)


### Tools

* [andOTP](https://github.com/andOTP/andOTP)
* [ShellCheck](https://www.shellcheck.net/)

### Must Read

* [pure bash bible](https://github.com/dylanaraps/pure-bash-bible)
