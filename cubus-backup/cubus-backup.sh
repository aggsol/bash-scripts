#!/usr/bin/env bash
#
# Helper script for borg backup
#
# GOTO TO USER CONFIG SECTION BELOW!
#
set -euo pipefail
IFS="$(printf '\n\t')"

# --- USER CONFIG START

# The pass file contains the secret key, any long random string will work
readonly passFile="$HOME/.config/.cubus-passphrase"
readonly quota="950G"
readonly backupName="backup"
readonly source="$HOME"
readonly repo="username@example.com:~/BorgRepo"
#readonly repo="/Path/to/your/BorgRepo/"
readonly ignoreFile="$HOME/.config/cubus-ignorelist"

# --- USER CONFIG END

export BORG_PASSCOMMAND="cat $passFile"
readonly version="1.2.1"
readonly scriptname="${0##*/}"
function printUsage()
{
    echo "${scriptname} ${version}"
    echo "Usage: ${scriptname} <ACTION> [PARAMS]"
    echo "EZ backup your home directory with borg"
    echo "EDIT ${scriptname} BEFORE FIRST USE!"
    echo ""
    echo "  init                       Initialize backup repository"
    echo "  backup                     Create a backup"
    echo "  mount <path>               Mount repository at <path>"
    echo "  prune                      Prune repository"
    echo "  check                      Check repository for errors"
    echo "  extract <archive> <path>   Extract archive"
    echo "  list                       List all archives"
    exit 0
}

function error()
{
    echo "[Error]: $*" >&2
}

if [[ ! -f "$passFile" ]]; then
    error "File $passFile is missing. Abort."
    exit 101
fi

if [[ $(stat -c %a "$passFile") != 400 ]]; then
    error "File $passFile has wrong permissions (required 400). Abort."
    exit 102
fi

if [[ ! -f "$ignoreFile" ]]; then
    error "File $ignoreFile is missing. Abort."
    exit 103
fi

# set default values
backup=false
help=false
initRepo=false
pruneRepo=false
checkRepo=false
extract=false
list=false
mountPoint="";
archive="";
archive_path="";

# input params
declare -a positional=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    "init")
    readonly initRepo=true
    shift # past argument
    ;;
    "backup")
    readonly backup=true
    shift # past argument
    ;;
    -h|--help)
    readonly help=true
    shift # past argument
    ;;
    "prune")
    readonly pruneRepo=true
    shift # past argument
    ;;
    "check")
    readonly checkRepo=true
    shift # past argument
    ;;
    "extract")
    readonly extract=true
    readonly archive="$2"
    readonly archive_path="$3"
    shift # past argument
    shift # past value archive
    shift # past value arhivce path
    ;;
    "list")
    readonly list=true
    shift # past argument
    ;;
    "mount")
    readonly mountPoint="$2"
    shift # past argument
    shift # past value mount point
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if [[ "$help" == true ]]; then
    printUsage
fi

if [[ "$initRepo" == true ]]; then
    borg init --encryption=repokey-blake2 --storage-quota "$quota" "$repo"
    exit 0
fi


function notify()
{
    if [[ -t 1 ]]; then echo "### $(basename -- "$0") ### -> $1"
    else # in x, notifications
        if   hash notify-send 2>/dev/null; then notify-send "$1"
        elif hash zenity 2>/dev/null; then { echo "message:$1" | zenity --notification --listen & }
        elif hash kdialog 2>/dev/null; then { kdialog --title "$1" --passivepopup "This popup will disappear in 5 seconds" 5 & }
        elif hash xmessage 2>/dev/null; then xmessage "$1" -timeout 5
        else echo "[$(date -u +"%FT%TZ")] $1" > "${HOME}/$(basename -- "$0")_notify.txt"
        fi
    fi
}

function prune_repo()
{
    notify "Pruning repo..."
    borg prune                          \
        --list                          \
        --glob-archives "${backupName}*"\
        --keep-daily    7               \
        --keep-weekly   4               \
        --keep-monthly  12              \
        --keep-yearly   10              \
        "${repo}"

    rc=$?
    if [[ $rc -gt 1 ]]; then
        notify "Prune finished with an error."
    elif [[ $rc -eq 1 ]]; then
        notify "Prune finished with a warning."
    else
        notify "Prune done."
    fi
}

if [[ "$backup" == true ]]; then
    notify "Start backup..."
    borg create \
        --verbose                       \
        --filter AME                    \
        --list                          \
        --stats                         \
        --compression auto,zstd         \
        --exclude-caches                \
        --exclude-from "$ignoreFile"    \
        "${repo}::${backupName}-{now:%Y-%m-%dT%H:%M}"   \
        "$source"

    rc=$?
    if [[ $rc -gt 1 ]]; then
        notify "Backup finished with an error."
    elif [[ $rc -eq 1 ]]; then
        notify "Backup finished with a warning."
    else
        notify "Backup done."
    fi

    prune_repo


elif [[ -n "$mountPoint" ]]; then
    notify "Mounting at $mountPoint..."
    borg mount \
        --glob-archives "${backupName}*" \
        --last 8 \
        "$repo" \
        "$mountPoint"

elif [[ "$pruneRepo" == true ]]; then
    prune_repo

elif [[ "$checkRepo" == true ]]; then
    echo "Checking repo..."
    borg check \
        --glob-archives "${backupName}*" \
         --show-rc \
        "${repo}"

elif [[ "$extract" == true ]]; then
    echo "Extracting full archive ${archive}"
    mkdir -p cubus_extract
    cd cubus_extract
    borg extract \
             --list \
             "${repo}::${archive}" \
             "${archive_path}"

elif [[ "$list" == true ]]; then
    echo "List archives"
    borg list \
        "${repo}"

else
    printUsage
fi
