#!/usr/bin/env bash
# Enrypted backup on OneDrive with Rclone and Restic
#
# 1. Install and setup Rclone for OneDrive
# 2. stall Restic (https://restic.net/)
#
set -euo pipefail
IFS="$(printf '\n\t')"
    
# --- USER CONFIG START

# The pass file contains the secret key, any long random string will work
readonly passFile="$HOME/.config/.onedrive-passphrase"
readonly tag="MyComputer"
readonly source="$HOME"
readonly repo="rclone:OneDrive:/MyBackup"
readonly ignoreFile="$HOME/.config/onedrive-ignorelist"

# --- USER CONFIG END
export RESTIC_PASSWORD_COMMAND="cat $passFile"
readonly version="1.1.0"
readonly scriptname="${0##*/}"
function printUsage()
{
    echo "${scriptname} ${version}"
    echo "Usage: ${scriptname} [ACTION]"
    echo "EZ backup your home directory with Restic and Rclone"
    echo "EDIT ${scriptname} BEFORE FIRST USE!"
    echo ""
    echo "Actions:"
    echo "backup            Create a backup"
    echo "check             Check repository for errors"
    echo "init              Initialize backup repository"
    echo "list              List snapshot in the repository"
    echo "mount <path>      Mount repository at <path>"
    echo "prune             Prune repository"
    exit 0
}

function error()
{
    echo "[Error]: $*" >&2
}

if [[ ! -f "$passFile" ]]; then
    error "File $passFile is missing. Abort."
    exit 101
fi

if [[ $(stat -c %a "$passFile") != 400 ]]; then
    error "File $passFile has wrong permissions (required 400). Abort."
    exit 102
fi

if [[ ! -f "$ignoreFile" ]]; then
    error "File $ignoreFile is missing. Abort."
    exit 103
fi

# set default values
backup=false
help=false
initRepo=false
pruneRepo=false
checkRepo=false
listRepo=false
mountPoint="";

# input params
declare -a positional=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    init)
    readonly initRepo=true
    shift # past argument
    ;;
    backup)
    readonly backup=true
    shift # past argument
    ;;
    -h|--help)
    readonly help=true
    shift # past argument
    ;;
    prune)
    readonly pruneRepo=true
    shift # past argument
    ;;
    check)
    readonly checkRepo=true
    shift # past argument
    ;;
    list)
    readonly listRepo=true
    shift # past argument
    ;;
    mount)
    readonly mountPoint="$2"
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if [[ "$help" == true ]]; then
    printUsage
fi

if [[ "$initRepo" == true ]]; then
    restic -r "$repo" init
    exit $?
fi

if [[ "$listRepo" == true ]]; then
    restic -r "$repo" snapshots
    exit $?
fi

function notify()
{
    if [[ -t 1 ]]; then echo "### $(basename -- "$0") ### -> $1"
    else # in x, notifications
        if   hash notify-send 2>/dev/null; then notify-send "$1"
        elif hash zenity 2>/dev/null; then { echo "message:$1" | zenity --notification --listen & }
        elif hash kdialog 2>/dev/null; then { kdialog --title "$1" --passivepopup "This popup will disappear in 5 seconds" 5 & }
        elif hash xmessage 2>/dev/null; then xmessage "$1" -timeout 5
        else echo "[$(date -u +"%FT%TZ")] $1" > "${HOME}/$(basename -- "$0")_notify.txt"
        fi
    fi
}

function prune_repo()
{
    notify "Pruning repo..."
    restic -r "$repo" forget \
        --prune               \
        --tag "${tag}"        \
        --keep-daily    7     \
        --keep-weekly   4     \
        --keep-monthly  12    \
        --keep-yearly   2

    rc=$?
    if [[ $rc -gt 1 ]]; then
        notify "Prune finished with an error."
    elif [[ $rc -eq 1 ]]; then
        notify "Prune finished with a warning."
    else
        notify "Prune done."
    fi
}

if [[ "$backup" == true ]]; then
    notify "Start backup..."
    restic -r "$repo" backup \
        --tag "$tag" \
        --verbose                       \
        --exclude-caches                \
        --exclude-file "$ignoreFile"    \
        "$source"

    rc=$?
    if [[ $rc -gt 1 ]]; then
        notify "Backup finished. Incomplete snapshot."
    elif [[ $rc -eq 1 ]]; then
        notify "Fatal error. No snapshot created."
    else
        notify "Backup done."
    fi

    prune_repo

elif [[ -n "$mountPoint" ]]; then
    notify "Mounting at $mountPoint..."
    restic -r "$repo" mount \
        "$mountPoint"

elif [[ "$pruneRepo" == true ]]; then
    prune_repo
    restic -r "$repo" check --read-data-subset=1%
    rc=$?
    notify "Prune done. rc={$rc}"

elif [[ "$checkRepo" == true ]]; then
    echo "Checking repo..."
    restic -r "$repo" check \
        --read-data-subset "50%"
    rc=$?
    notify "Check done. rc={$rc}"

else
    printUsage
fi
