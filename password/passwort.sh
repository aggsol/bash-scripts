#!/usr/bin/env bash
# Erzeuge deutsche Passwörter, die leicht zu merken sind
#
# Deutsches Wörtbuch wird benötigt: apt install wngerman
#
set -euo pipefail
IFS="$(printf '\n\t')"

DICT="/usr/share/dict/ngerman"
SHOW_HELP=false
SEPARATOR="-"
NUM=8

while [[ $# -gt 0 ]]
do
KEY="$1"

case $KEY in
    -h|--help)
    SHOW_HELP=true
    ;;
    -d|--dict)
    DICT="$2"
    shift # past argument
    ;;
    -s|--separator)
    SEPARATOR="$2"
    shift # past argument
    ;;
    -n|--num)
    NUM="$2"
    shift
    ;;
    *)
    echo "Unbekannte Option $KEY"
    exit 1
    ;;
esac
shift # past argument or value
done

if [[ $SHOW_HELP = true ]];
then
    echo "Usage: passwort.sh <OPTIONEN>"
    echo "Erzeuge ein deutsches Passwort aus vier Worten ohne Umlaute oder Eszett"
    echo "  --separator, -s <CHAR>    Trenne Worte mit <CHAR> Zeichen, default: $SEPARATOR"
    echo "  --num, -n <NUM>           Erzeuge <NUM> Passworte, default: $NUM"
    echo "  --dict, -d <DATEI>        Nutze Worte aus <DATEI>, default: $DICT"
    echo "  --help, -h                Zeige diesen Hilfetext"
    exit 0
fi

if [[ ! -f $DICT ]];
then
    echo "Wörterbuch nicht gefunden $DICT"
    exit 1
fi

function generateWord
{
    WORDS=$(grep "^[^']\{3,6\}$" "$DICT" | shuf -n4 --random-source=/dev/urandom )
    WORDS=$(echo $WORDS)
    WORDS=$(echo "$WORDS" | sed s/ä/ae/g | sed s/ü/ue/g | sed s/ö/oe/g | sed s/ß/ss/g | sed -e "s/\b\(.\)/\u\1/g")
    WORDS=$(echo "$WORDS" | tr ' ' "$SEPARATOR")

    echo "$WORDS"
}

for (( COUNT=1; COUNT<=NUM; COUNT++ ))
do
   generateWord
done