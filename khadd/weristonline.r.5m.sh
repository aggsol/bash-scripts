#!/bin/bash
set -euo pipefail
IFS="$(printf '\n\t')"
# KHADD Tools für Argos/BitBar
#`7MMF' `YMM' `7MMF'  `7MMF'      db      `7MM"""Yb.   `7MM"""Yb.
#  MM   .M'     MM      MM       ;MM:       MM    `Yb.   MM    `Yb.
#  MM .d"       MM      MM      ,V^MM.      MM     `Mb   MM     `Mb
#  MMMMM.       MMmmmmmmMM     ,M  `MM      MM      MM   MM      MM
#  MM  VMA      MM      MM     AbmmmqMA     MM     ,MP   MM     ,MP
#  MM   `MM.    MM      MM    A'     VML    MM    ,dP'   MM    ,dP'
#.JMML.   MMb..JMML.  .JMML..AMA.   .AMMA..JMMmmmdP'   .JMMmmmdP'

#  Nutzt den M!service von Christopher Reitz. Danke!
#  https://nerds.berlin/mservice/
#  https://github.com/Stitch7/mservice

# CONFIG ANFANG
# Nutzernamen als Ticker oder als Dropdown-Menü anzeigen

readonly dropdown=false

# CONFIG ENDE

readonly version="1.0.0-beta"
readonly api="https://nerds.berlin/mservice/users/online"
readonly khaddIcon=$(curl -s "https://www.maniac-forum.de/forum/favicon.ico" | base64 -w 0)
readonly tmpfile=$(mktemp)
#tmpfile="online.json"

wget -q "$api" -O "$tmpfile"
readonly names=$(jq -r '.users[]|"\(.username) | length=20"' "$tmpfile")
readonly count=$(jq -r '.count.total' "$tmpfile")

echo "<span color='#FFFF00' weight='normal'>M!</span> ${count} online | length=20"

if $dropdown; then
    echo "---"
fi

echo "$names"

rm -f "$tmpfile"
exit 0