#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"
# KHADD Tools für Argos
#`7MMF' `YMM' `7MMF'  `7MMF'      db      `7MM"""Yb.   `7MM"""Yb.
#  MM   .M'     MM      MM       ;MM:       MM    `Yb.   MM    `Yb.
#  MM .d"       MM      MM      ,V^MM.      MM     `Mb   MM     `Mb
#  MMMMM.       MMmmmmmmMM     ,M  `MM      MM      MM   MM      MM
#  MM  VMA      MM      MM     AbmmmqMA     MM     ,MP   MM     ,MP
#  MM   `MM.    MM      MM    A'     VML    MM    ,dP'   MM    ,dP'
#.JMML.   MMb..JMML.  .JMML..AMA.   .AMMA..JMMmmmdP'   .JMMmmmdP'

#  Nutzt den M!service von Christopher Reitz. Danke!
#  https://nerds.berlin/mservice/
#  https://github.com/Stitch7/mservice

# USER CONFIG ANFANG
# Hier den gewünschten Nutzernamen eintragen

readonly username="Leviathan"
readonly cacheDir="$HOME/.cache/khadd"

# USERT CONFIG ENDE

readonly version="1.0.0-beta"
readonly responsesApi="https://nerds.berlin/mservice/user/${username}/responses"
readonly userOnlineApi="https://nerds.berlin/mservice/users/online"
readonly oldIfs=$IFS

if [[ ! -d "$cacheDir" ]]; then
    mkdir -p "$cacheDir"
fi

# M! Forum Icon
readonly iconFile="$cacheDir/khadd.ico"
if [[ ! -f "$iconFile" ]]; then
    curl -s "https://www.maniac-forum.de/forum/favicon.ico" | base64 -w 0 > "$iconFile"
fi;
readonly mIcon=$(cat "$iconFile")

tmpfile=$(mktemp)

# User Online
wget -q "$userOnlineApi" -O "$tmpfile"
readonly count=$(jq -r '.count.total' "$tmpfile")

echo "<span color='#FFFF00' weight='normal'>M!</span> $count online"
echo "---"

# Antworten
echo "Antworten für $username"
wget -q "$responsesApi" -O "$tmpfile"
readonly topAnswers=$(jq -r 'sort_by(.date)|reverse[0:4][]|"\(.boardId)|\(.messageId)|\(.username)|\(.subject)"' "$tmpfile")

IFS='|'
while read -r boardId messageId poster subject;
do
    echo "---"
    echo "${subject:0:20}"
    link="https://maniac-forum.de/forum/pxmboard.php?mode=message&brdid=${boardId}&msgid=${messageId}"
    echo "Antwort von $poster lesen | image='$mIcon' imageWidth=20 href='$link'"

done <<< "$topAnswers"
IFS=$oldIfs

rm -f "$tmpfile"
exit 0
