#!/usr/bin/env bash
#
# Bash script that disables the touchpad if a mouse is present.
#
set -e
set -u
# set -x

MOUSE_ID=$(xinput list | grep -Eio 'mouse\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}')
# echo "Mouse id: ${MOUSE_ID}"
if [[ -z ${MOUSE_ID} ]];
then
    echo "No mouse found"
    exit 0
fi

TOUCHPAD_ID=$(xinput list | grep -Eio 'touchpad\s*id\=[0-9]{1,2}' | grep -Eo '[0-9]{1,2}')
if [[ -z {TOUCHPAD_ID} ]];
then
    echo "No touchpad found"
    exit 1
fi

TOUCHPAD_STATE=`xinput list-props ${TOUCHPAD_ID}|grep 'Device Enabled'|awk '{print $4}'`
if [[ ${TOUCHPAD_STATE} -eq 1 ]];
then
    xinput disable ${TOUCHPAD_ID}
    echo "Touchpad disabled."
else
    echo "Touchpad already disabled."
fi
