#!/usr/bin/env bash
# --- CONFIG START ---
readonly repo="$HOME/Cloud/"
# --- CONFIG END ---

if[[ ! -d "$repo" ]];
then
    echo "Sync | iconName=sync-error color=#ff0000"
    echo "Error: Check user configuration! | length=16"
    echo "---"
    echo "Edit user configuration | terminal=false bash='xdg-open $0' iconName=accessories-text-editor"
    exit 0
fi

# git log --name-status -10 --pretty=oneline --format=%cd

timestamp=$(git -C ${repo} log -1 --format=%cd --date=relative)
echo "Cloud Sync: ${timestamp} | iconName=weather-overcast"
echo "---"
git -C "$repo" log -10 --pretty=oneline --abbrev-commit --format=%cd
echo "---"
echo "Refresh | terminal=true bash='touch $0;' terminal=false iconName=view-refresh"
