#!/usr/bin/env bash
# Inspired by https://github.com/daniperez/gnome-mfa
# Works with andOTP JSON files

set -euo pipefail
IFS="$(printf '\n\t')"

# ---  USER CONFIG START
readonly JSON_FILE="${HOME}/.config/otp_accounts.json"
# --- USER CONFIG END

argosVersion=${ARGOS_VERSION:-1.10}
argosMenuOpen=${ARGOS_MENU_OPEN:-false}

#echo "argosVersion='$argosVersion'"
#echo "argosMenuOpen='$argosMenuOpen'"

error()
{
    if [[ -z "$argosVersion" ]]; then
        echo "[Error]: $*" >&2
    else
        echo ":key: OTP | color='red'"
        echo "---"
        echo "$* | color='red'"
    fi
}

if [[ ! -f "$JSON_FILE" ]]; then
  error "OTP accounts file $JSON_FILE is missing. Abort."
  exit 101
fi


if [[ $(stat -c %a "$JSON_FILE") != 400 ]]; then
  error "OTP accounts file has wrong permissions (required 400). Abort."
  exit 102
fi

checkTool()
{
    if ! type -p "$1" &>/dev/null; then
        error "$1 is not installed. Abort."
        exit 1
    fi
}

checkTool oathtool
checkTool jq
checkTool sed

if [[ -z "$argosVersion" ]]; then
    echo "OTP        Account"
else
    checkTool xclip
    echo ":key: OTP | refresh=true"
    echo "---"
fi

if [[ "$argosMenuOpen" == "true" ]]; then
    for ROW in $(jq -c 'sort_by(.label) | .[]' "${JSON_FILE}"); do
        each()
        {
            echo "${ROW}" | jq -r "$1"
        }

        LABEL=$(each '.label')
        SECRET=$(each '.secret')
        CODE=$(oathtool --totp -b "$SECRET")

        if [[ -v ARGOS_VERSION ]]; then

            echo "${CODE}   ${LABEL}  | font=monospace bash='echo -n ${CODE} | xclip -selection c && echo -n ${CODE} | xclip' terminal=false"
        else
            echo "${CODE}   ${LABEL}"
        fi
    done
else
    echo "Loading..."
fi
