#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

version="1.0.0"
function usage()
{
    echo "Usage: yt-audio-play.sh [OPTION]"
    echo "Download radio plays from YouTube as MP3 file"
    echo ""
    echo "  -u, --url <URL>     Download from YouTube URL"
    echo "  -i, --id <ID>       Download from the video ID"
    echo "      --version       Print version"
    echo "  -h, --help          Show this help"
}

function error()
{
    echo "Error: $*" >&2
}

function checkTools()
{
    local -i missing=0
    for cmd; do
        if ! hash "$1" >/dev/null 2>&1 "$cmd" ; then
            error "'$cmd' is missing"
            missing=1
        fi
    done

    if  [[ $missing -ne 0 ]]; then
        echo "Install missing dependencies listed above."
        echo "* youtube-dl: https://github.com/rg3/youtube-dl"
        echo "* apt install eyeD3 jq imagemagick"
        exit 103
    fi
}

checkTools youtube-dl jq convert eyeD3

# set default values
videoId=""
url=""
help=false
showVersion=false
target="$PWD"

declare -a positional=()

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--id)
    videoId="$2"
    shift # past argument
    shift # past value
    ;;
    -u|--url)
    url="$2"
    shift # past argument
    shift # past value
    ;;
    --version)
    showVersion=true
    shift # past argument
    ;;
    -h|--help)
    help=true
    shift # past argument
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

if $help ; then
    usage
    exit
fi

if $showVersion ; then
    echo "$version"
    exit
fi

if [[ ! -z $url ]]; then

    # taken from: https://ubuntuforums.org/showthread.php?t=1966977
    videoId=$(echo "${url}" | tr "?&#" "\\n" | grep "^v=" | cut -d "=" -f 2)

fi

if [[ -z ${videoId} ]]; then
    error "Missing parameters"
    exit 102
fi

echo "VIDEO ID = ${videoId}"

tmpfolder=$(mktemp --directory)
#echo "Create folder $tmpfolder"
cd "$tmpfolder"

function cleanup() {
    #echo "Cleaning stuff up..."
    rm -rf "$tmpfolder"
    exit
}

trap cleanup INT TERM ERR

youtube-dl -o '%(title)s.%(ext)s' --write-thumbnail --restrict-filenames --write-info-json --extract-audio --audio-format mp3 "${videoId}"

jsonFile=$(ls ./*.json)
title=$(jq -r '.title' $jsonFile)
mp3file=$(ls ./*.mp3)
thumbnail=$(ls ./*.jpg)

convert -define jpeg:size=512x512 "$thumbnail"  -thumbnail '@400000' -gravity center -background black -extent 200x200  cover.jpg

eyeD3 --itunes --add-image=cover.jpg:FRONT_COVER: --title="$title" --genre=12 "$mp3file"

cp -f ./*.mp3 "$target"