#!/usr/bin/env bash
set -euo pipefail
# Setup bitpocket first: https://github.com/sickill/bitpocket

# --- USER CONFIG START ---
readonly repo="$HOME/BitPocket/"
readonly pocketName="BitPocket"
# --- USER CONFIG END ---


if[[ ! -d "$repo" ]];
then
    echo "$pocketName | iconName=sync-error color=#ff0000"
    echo "Error: Check user configuration! | length=16"
    echo "---"
    echo "Edit user configuration | terminal=false bash='xdg-open $0' iconName=accessories-text-editor"
    exit 0
fi

echo "%pocketName"
echo "---"
echo "Sync | terminal=true bash='(cd $repo && bitpocket cron)' iconName=view-refresh"
echo "---"
logoutput=$( head -n 20 "${repo}.bitpocket/log" )
echo "$logoutput | font=monospace"