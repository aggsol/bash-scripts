#!/usr/bin/env bash
# Based on: https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
#
set -euo pipefail
IFS="$(printf '\n\t')"

# set default values
extension=""
searchpath=""
libpath=""
default=false

declare -a positional=()

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -e|--extension)
    readonly extension="$2"
    shift # past argument
    shift # past value
    ;;
    -s|--searchpath)
    readonly searchpath="$2"
    shift # past argument
    shift # past value
    ;;
    -l|--lib)
    readonly libpath="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    readonly default=true
    shift # past argument
    ;;
    *)    # unknown option
    positional+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${#positional[@]}" # restore positional parameters

echo "FILE EXTENSION  = ${extension}"
echo "SEARCH PATH     = ${searchpath}"
echo "LIBRARY PATH    = ${libpath}"
echo "DEFAULT         = ${default}"
