#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

tmpfile=$(mktemp)
echo "Create file $tmpfile"
touch $tmpfile

function cleanup() {
    echo "Cleaning stuff up..."
    rm -fv $tmpfile
    exit
}

trap cleanup INT TERM ERR
echo ' --- press ENTER to close or terminate --- '

read var
cleanup