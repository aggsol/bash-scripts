#!/usr/bin/env bash
# Based on https://github.com/yeKcim/my_nautilus_scripts
# License: GPLv3
#
# Send notifications. If not run in a terminal a desktop notification is sent.
# If no desktop is available then a file in $HOME is written
#
set -euo pipefail
IFS="$(printf '\n\t')"

function notify()
{
    if   hash notify-send 2>/dev/null; then notify-send "$1"
    elif hash zenity 2>/dev/null; then { echo "message:$1" | zenity --notification --listen & }
    elif hash kdialog 2>/dev/null; then { kdialog --title "$1" --passivepopup "This popup will disappear in 5 seconds" 5 & }
    elif hash xmessage 2>/dev/null; then xmessage "$1" -timeout 5
    else echo "[$(date -u +"%FT%TZ")] $1" > "${HOME}/$(basename -- "$0")_notify.txt"
    fi
}
message=${1:-}
notify "$message"
