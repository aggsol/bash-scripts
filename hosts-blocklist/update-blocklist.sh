#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

today=$(date '+%Y-%m-%d')
config_file="$HOME/.config/local.hosts"
tmp_hosts="/tmp/hosts"
backup="/etc/hosts.backup"

if [[ ! -f "$backup" ]]; then
	echo "Creating backup /etc/hosts -> $backup"
	cp -f /etc/hosts "$backup"

	if [[ ! -f "$config_file" ]]; then
		echo "Init config $config_file"
		cp -f /etc/hosts "$config_file"
	fi
fi

if [[ ! -f "$config_file" ]]; then
	echo "Missing $config_file"
	exit 1
fi

wget -nv -O "$tmp_hosts" https://raw.githubusercontent.com/StevenBlack/hosts/master/alternates/gambling-porn/hosts

cat "$config_file" > /etc/hosts
cat "$tmp_hosts" >> /etc/hosts
