#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

function error()
{
  echo "[Error]: $@" >&2
}

#SCRIPT_NAME="${0##*/}"
readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ "$PWD" == "$SCRIPT_DIR" ]]; then
    error "Do not encrypt the git-crypto repository. Abort."
    exit 666
fi

function checkTools()
{
    local -i missing=0
    for cmd; do
        if ! hash "$1" >/dev/null 2>&1 "$cmd" ; then
            error "$cmd is missing"
            missing=1
        fi
    done

    if  [[ $missing -ne 0 ]]; then
        error "Install missing dependencies listed above."
        exit 103
    fi
}

checkTools git openssl

readonly CRYPTO_DIR="$PWD/.git/crypto"

mkdir -p "$CRYPTO_DIR"

readonly PASS_FILE="$CRYPTO_DIR/password"

if [[ -f "$PASS_FILE" ]]; then
    error "Repository is already encrypted. Abort."
    exit 101
fi

touch "$PASS_FILE"
chmod 600 "$PASS_FILE"
echo $(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 64 ; echo '') > "$PASS_FILE"

cp "$SCRIPT_DIR/git-crypto-smudge-filter.sh" "$CRYPTO_DIR/"
cp "$SCRIPT_DIR/git-crypto-clean-filter.sh" "$CRYPTO_DIR/"

git config filter.gitcrypto.required true
git config filter.gitcrypto.smudge "$CRYPTO_DIR/git-crypto-smudge-filter.sh %f"
git config filter.gitcrypto.clean "$CRYPTO_DIR/git-crypto-clean-filter.sh %f"
git config merge.renormalize true

readonly ATTR_FILE="$PWD/.gitattributes"
touch "$ATTR_FILE"
echo "* filter=gitcrypto -diff -delta merge=binary" > "$ATTR_FILE"
echo ".gitattributes filter=" >> "$ATTR_FILE"
git add "$ATTR_FILE"
git commit -m "git-crypto initial commit"

echo "Your password is stored in $PASS_FILE"
echo ""
echo "IF YOU LOSE OR CHANGE THE PASSWORD THEN ALL PREVIOUSLY ENCRYPTED DATA IS LOST!"
echo ""
echo "Done."