#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

function error()
{
  echo "[Error]: $@" >&2
}

readonly PASS_FILE=".git/crypto/password"

if [[ ! -f "$PASS_FILE" ]]; then
  error "Password file $PASS_FILE is missing. Abort."
  exit 101
fi


if [[ $(stat -c %a "$PASS_FILE") != 600 ]]; then
  error "Password file has wrong permissions. Abort."
  exit 102
fi

openssl enc -base64 -aes-256-gcm -pass file:"$PASS_FILE"