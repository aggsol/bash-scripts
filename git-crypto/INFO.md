# git-crypto

Use smudge and clean filters to encrypt files before commiting into a git repository

* Filenames are not encrypted (information leak)
* Commit messages are not encrypted
* Use AES-256 in GCM mode provided by OpenSSL
* Password is stored in .git/crypto/password
* Files are base64 encoded
* git cannot check if content of a file has changed

## TODO

* Use AES-SIV (check for new OpenSSL)