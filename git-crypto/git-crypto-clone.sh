#!/usr/bin/env bash
set -euo pipefail
IFS="$(printf '\n\t')"

function error()
{
  echo "[Error]: $@" >&2
}

function checkTools()
{
  local -i missing=0
  for cmd; do
    if ! hash "$1" >/dev/null 2>&1 "$cmd" ; then
      error "$cmd is missing"
      missing=1
    fi
  done

  if  [[ $missing -ne 0 ]]; then
    error "Install missing dependencies listed above."
    exit 103
  fi
}

checkTools git openssl

readonly SCRIPT_NAME="${0##*/}"
readonly SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [[ $# -ne 3 ]]; then
  error "Usage $SCRIPT_NAME <repo-url> <name> <password-file>"
  exit 104
fi

readonly REPO_URL=$1
readonly REPO_NAME=$2
readonly PASSWORD_SOURCE=$(readlink --canonicalize "$3")

if [[ ! -f "$PASSWORD_SOURCE" ]]; then
  error "Password file is not a valid file. Abort."
  exit 101
fi

if [[ $(stat -c %a "$PASSWORD_SOURCE") != 600 ]]; then
  error "Password file has wrong permissions. Expected 600. Abort."
  exit 102
fi

readonly CRYPTO_DIR=".git/crypto"
readonly PASS_FILE="$CRYPTO_DIR/password"

mkdir -p "$REPO_NAME"
cd "$REPO_NAME"

git clone --progress --no-checkout "$REPO_URL" .

mkdir -p "$CRYPTO_DIR"

cat "$PASSWORD_SOURCE" > "$PASS_FILE"
chmod 600 $PASS_FILE

cp "$SCRIPT_DIR/git-crypto-smudge-filter.sh" "$CRYPTO_DIR/"
cp "$SCRIPT_DIR/git-crypto-clean-filter.sh" "$CRYPTO_DIR/"

git config filter.gitcrypto.required true
git config filter.gitcrypto.smudge "$CRYPTO_DIR/git-crypto-smudge-filter.sh %f"
git config filter.gitcrypto.clean "$CRYPTO_DIR/git-crypto-clean-filter.sh %f"
git config merge.renormalize true

git checkout --

 echo "Done."
