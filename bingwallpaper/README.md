# Bing Wallpapers For Linux

**Forked from https://github.com/whizzzkid/bing-wallpapers-for-linux**

Set the current Bing image as desktop wallpaper.

## Supported Desktop Environments

- Gnome
- Cinnamon
- Unity
- Xfce

## Using Script directly.

Just download or clone this repo. Run:

    $ bingwallpaper



## Setting Up Cron

To setup regular checks for new wallapers, edit crontab for the current user, using:

    $ crontab -u $USER -e

and add this line:

    0 */6 * * * bingwallpaper -1 > /dev/null 2>&1

This will run every 6 hours. Keep in mind that cron works best if the system is running all the time.

## License
GPLv2

